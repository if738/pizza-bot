package com.student.Pizza.bot.handler.impl;

import com.student.Pizza.bot.entity.Menu;
import com.student.Pizza.bot.handler.interfaces.CalculatorHandler;
import com.student.Pizza.bot.repository.db.MenuEditRepository;
import com.student.Pizza.bot.repository.list_realisation.interfaces.ItemsRepository;
import com.student.Pizza.bot.repository.list_realisation.interfaces.PricesRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class CalculatorHandlerImpl implements CalculatorHandler {

    private final MenuEditRepository menuEditRepository;
    private final ItemsRepository<Menu> orderedItemsRepository;
    @Autowired
    @Qualifier("netPriceRepositoryImpl")
    private PricesRepository netPriceRepository;
    @Qualifier("discountPriceRepositoryImpl")
    @Autowired
    private PricesRepository discountPriceRepository;
    @Autowired
    @Qualifier("totalPriceRepositoryImpl")
    private PricesRepository totalPriceRepository;

    @Override
    public void sumOrder() {
        List<String> orderCheckList = getOrderCheckList();
        List<Menu> orderInfo = orderCheckList.stream().map(menuEditRepository::findByName)
                .collect(Collectors.toList());
        List<Float> pricesList = orderInfo.stream().map(Menu::getPrice).collect(Collectors.toList());
//        float sum = (float) pricesList.stream().mapToDouble(Float::longValue).sum();
//        netPriceRepository.put(sum);
        //TODO Вова: почему не захотело кастить в 'F'loat
//        float totalPrice =netPriceRepository.get() - freePizzaRepository.get() - netPriceRepository.get();
//        totalPriceRepository.put(totalPrice);
    }

    private List<String> getOrderCheckList() {
        return orderedItemsRepository.getAll().stream().map(Menu::getName).collect(Collectors.toList());
    }

}