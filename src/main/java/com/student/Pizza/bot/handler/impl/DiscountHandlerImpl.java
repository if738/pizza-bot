package com.student.Pizza.bot.handler.impl;

import com.student.Pizza.bot.entity.DiscountPercent;
import com.student.Pizza.bot.handler.interfaces.DiscountHandler;
import com.student.Pizza.bot.repository.db.PercentEditRepository;
import com.student.Pizza.bot.repository.list_realisation.interfaces.PricesRepository;
import com.student.Pizza.bot.view.InputView;
import com.student.Pizza.bot.view.ShowView;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class DiscountHandlerImpl implements DiscountHandler {

    private final ShowView showView;
    private final InputView inputView;
    private final PercentEditRepository percentEditRepository;
    @Autowired
    @Qualifier("totalPriceRepositoryImpl")
    private PricesRepository totalPriceRepositoryImpl;
    @Autowired
    @Qualifier("discountPriceRepositoryImpl")
    private PricesRepository discountPrice;

    @Override
    public void discount() {
        long id = 1;
        showDeliverSentence();
        String cardNumberTry = inputView.input().nextLine();
        String stringToInt = cardNumberTry.replaceAll("\\D", "");
        if (stringToInt.isEmpty()) return;
        Integer cardNumber = Integer.valueOf(stringToInt);
        if (getDiscountIfValid(id, cardNumber)) return;
        getValidNumber(id, cardNumber);
    }

    private void getValidNumber(long id, Integer cardNumber) {
        String cardNumberTry;
        if (cardNumber > 0) {
            do {
                if (getDiscountIfValid(id, cardNumber)) return;
                showInvalidInput();
                cardNumberTry = inputView.input().nextLine();
                cardNumber = Integer.valueOf(cardNumberTry.replaceAll("\\D", ""));
                if (getDiscountIfValid(id, cardNumber)) return;
            } while (cardNumber < 100000 || cardNumber > 999999);
        }
    }

    private void showInvalidInput() {
        showView.show("False discount card number, please" +
                "\ninput 6 digit number");
    }

    private boolean getDiscountIfValid(long id, Integer cardNumber) {
        if (cardNumber > 99999 && cardNumber < 1000000) {
            Optional<DiscountPercent> a = percentEditRepository.findById(id);
            if (isPresentPercent(a)) return true;
        }
        return false;
    }

    private void showDeliverSentence() {
        showView.show("-------------------------------------------------------------------------------" +
                "\nYour discount card number??" +
                "\nInput numbers if you have or some characters if not");
    }

    private boolean isPresentPercent(Optional<DiscountPercent> a) {
        if (a.isPresent()) {
            float totalPrice = totalPriceRepositoryImpl.get();
            float discount = totalPrice - (totalPrice * (100 - a.get().getPercent())) / 100;
            totalPriceRepositoryImpl.delete(discount);
            discountPrice.put(discount);
            showView.show("discount: " + discountPrice.get());
            return true;
        }
        return false;
    }

}