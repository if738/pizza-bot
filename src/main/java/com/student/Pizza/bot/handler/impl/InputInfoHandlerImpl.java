package com.student.Pizza.bot.handler.impl;

import com.student.Pizza.bot.handler.interfaces.InputInfoHandler;
import com.student.Pizza.bot.repository.db.MenuEditRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class InputInfoHandlerImpl implements InputInfoHandler {

    @Autowired
    private MenuEditRepository repository;

    @Override
    public List<String> getOrderInfo(String getStringFromConsole, boolean validity) {

        List<String> validityInfo = new ArrayList<>(getSeparatedElements(getStringFromConsole));

        return getOrderInfo(validity, validityInfo);

    }

    @Override
    public List<String> getOrderInfo(String getStringFromConsole) {
        return getSeparatedElements(getStringFromConsole);
    }

    @Override
    public List<String> getOrderInfo(List<String> order) {
        return order.stream().filter(v ->repository.findByName(v) != null).collect(Collectors.toList());
    }

    @Override
    public List<String> getValidItems(String order) {
        return new ArrayList<>(getOrderInfo(order,true));
    }

    private List<String> getOrderInfo(boolean validity, List<String> validityInfo) {
        return validityInfo.stream().filter(v -> {
            if (validity) {
                return repository.findByName(v) != null;
            }
            return repository.findByName(v) == null;
        }).collect(Collectors.toList());
    }

    private List<String> getSeparatedElements(String getSumFromDB) {

        return Arrays.asList(getSumFromDB.toLowerCase().split(" "));

    }

}