package com.student.Pizza.bot.handler.impl;

import com.student.Pizza.bot.handler.interfaces.SentenceHandler;
import com.student.Pizza.bot.view.ShowView;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class SentenceHandlerImpl implements SentenceHandler {

    private final ShowView showView;
    private final AdditionalOrderItemsHandlerImpl additionalItemsHandler;

    @Override
    public void sentence() {
        showView.show("-------------------------------------------------------------------------------" +
                "\nMaybe you want to order cool lemonade " +
                "\nor something else?");
        additionalItemsHandler.setMoreItems();
    }

}
