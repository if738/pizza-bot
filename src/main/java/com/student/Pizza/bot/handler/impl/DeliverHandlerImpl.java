package com.student.Pizza.bot.handler.impl;

import com.student.Pizza.bot.entity.FreePizza;
import com.student.Pizza.bot.entity.Menu;
import com.student.Pizza.bot.handler.interfaces.AdditionaOrderlItemsHandler;
import com.student.Pizza.bot.handler.interfaces.DeliverHandler;
import com.student.Pizza.bot.repository.list_realisation.interfaces.ItemsRepository;
import com.student.Pizza.bot.repository.list_realisation.interfaces.PricesRepository;
import com.student.Pizza.bot.view.ShowView;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DeliverHandlerImpl implements DeliverHandler {
    private final AdditionalOrderItemsHandlerImpl additionalItemsHandler;
    private final ShowView showView;

    @Autowired
    @Qualifier("totalPriceRepositoryImpl")
    private PricesRepository totalPriceRepository;
    @Autowired
    @Qualifier("freePizzaRepositoryImpl")
    private ItemsRepository<FreePizza> freePizzaRepository;
    @Autowired
    @Qualifier("orderedItemsRepositoryImpl")
    private ItemsRepository<Menu> orderedItemsRepository;
    @Autowired
    @Qualifier("deliverPriceRepositoryImpl")
    private PricesRepository deliverPriceRepository;


    @Override
    public void deliver() {
        while (totalPriceRepository.get() >= 270 && totalPriceRepository.get() < 300) {
            showView.show("-------------------------------------------------------------------------------" +
                    "\nYour order is cost" + totalPriceRepository.get() +
                    "\ndelivery will cost additional 30.00." +
                    "\nYou can order something to have free delivery (start after 300.00)");
            int freePizzaCount = freePizzaRepository.getAll().size();
            int orderedItemsCount = orderedItemsRepository.getAll().size();
            additionalItemsHandler.setMoreItems();
            if (isEdit(freePizzaCount, orderedItemsCount)) {
                return;
            }
        }
        putIs();
    }

    private boolean isEdit(int freePizzaCount, int orderedItemsCount) {
        return freePizzaCount == freePizzaRepository.getAll().size() && orderedItemsCount == orderedItemsRepository.getAll().size();
    }

    private void putIs(){
        if (totalPriceRepository.get() < 300){
            deliverPriceRepository.put((float) 30);
            totalPriceRepository.put((float) 30);
        }
    }

}
