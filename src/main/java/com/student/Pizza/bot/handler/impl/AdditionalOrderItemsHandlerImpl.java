package com.student.Pizza.bot.handler.impl;

import com.student.Pizza.bot.entity.Menu;
import com.student.Pizza.bot.entity.Type;
import com.student.Pizza.bot.handler.interfaces.AdditionaOrderlItemsHandler;
import com.student.Pizza.bot.handler.interfaces.FreePizzaHandler;
import com.student.Pizza.bot.handler.interfaces.InputInfoHandler;
import com.student.Pizza.bot.repository.db.MenuEditRepository;;
import com.student.Pizza.bot.repository.list_realisation.interfaces.ItemsRepository;
import com.student.Pizza.bot.repository.list_realisation.interfaces.PricesRepository;
import com.student.Pizza.bot.view.InputView;
import com.student.Pizza.bot.view.ShowView;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class AdditionalOrderItemsHandlerImpl implements AdditionaOrderlItemsHandler {

    private final InputView inputView;
    private final ShowView showView;
    private final InputInfoHandler inputInfoHandler;
    private final MenuEditRepository menuEditRepository;
    private final FreePizzaHandler freePizzaHandler;
    @Autowired
    @Qualifier("orderedItemsRepositoryImpl")
    private ItemsRepository<Menu> orderedItemsRepository;
    @Autowired
    @Qualifier("totalPriceRepositoryImpl")
    private PricesRepository totalPriceRepository;

    @Override
    public void setMoreItems() {
        String order;
        order = getTrueItems();
        if (order == null) return;
        List<Menu> additionalOrder;
        additionalOrder = inputInfoHandler.getValidItems(order).stream()
                .map(menuEditRepository::findByName).collect(Collectors.toList());
        orderedItemsRepository.putAll(additionalOrder);
        showView.show("-------------------------------------------------------------------------------" +
                "\nokay, your current order is: " + orderedItemsRepository.getAll()
                .stream().map(Menu::getName).collect(Collectors.joining(" ")));
    setAdditionalPrices((float) additionalOrder.stream().mapToDouble(Menu::getPrice).sum());
    check();
    }

    private void check(){
        freePizzaHandler.thirdPizzaIsFree();
        if (((orderedItemsRepository.getAll().stream().filter(m -> m.getType() == Type.PIZZA).count() + 1) % 3) == 0){
            showView.show("-------------------------------------------------------------------------------" +
                    "\nWe have special offer \"Each third pizza is free\". " +
                    "\nYou may order one more pizza free, input pizzas or input more items");
            setMoreItems();
        }
    }

    private void setAdditionalPrices(float additionalPrice){
        totalPriceRepository.put(additionalPrice);
    }

    private String getTrueItems() {
        String order;
        do {
            showView.show("-------------------------------------------------------------------------------" +
                    "\nPress enter to accept current: "
                    + inputInfoHandler.getOrderInfo(orderedItemsRepository.getAll()
                    .stream().map(Menu::getName).collect(Collectors.joining(" ")))
                    + " or input additional items");
            order = setMoreItemsTemp();
            if (order == null) break;
        }
        while (inputInfoHandler.getOrderInfo(order, false).size() > 0);
        return order;
    }

    private String setMoreItemsTemp() {
        String order = inputView.input().nextLine();
        if (isaBoolean(order))
            return null;
        if (inputInfoHandler.getOrderInfo(order, false).size() > 0)
            System.out.println("-------------------------------------------------------------------------------" +
                    "\ninvalid input, try again");
        return order;
    }

    private boolean isaBoolean(String order) {
        List<String> AllRepo = orderedItemsRepository.getAll().stream().map(Menu::getName).collect(Collectors.toList());
        return order.equals("") && inputInfoHandler.getOrderInfo(AllRepo).size() > 0;
    }

}