package com.student.Pizza.bot.handler.impl;

import com.student.Pizza.bot.entity.FreePizza;
import com.student.Pizza.bot.handler.interfaces.CheckHandler;
import com.student.Pizza.bot.repository.list_realisation.interfaces.ItemsRepository;
import com.student.Pizza.bot.repository.list_realisation.interfaces.PricesRepository;
import com.student.Pizza.bot.view.ShowView;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ResultCheckHandlerImpl implements CheckHandler {

    private final ShowView showView;
    @Autowired
    @Qualifier("deliverPriceRepositoryImpl")
    private PricesRepository deliverPriceRepository;
    @Autowired
    @Qualifier("discountPriceRepositoryImpl")
    private PricesRepository discountPrice;
    @Autowired
    @Qualifier("freePizzaRepositoryImpl")
    private ItemsRepository<FreePizza> actionPizzaRepository;
    @Autowired
    @Qualifier("totalPriceRepositoryImpl")
    private PricesRepository totalPriceRepository;
    @Autowired
    @Qualifier("netPriceRepositoryImpl")
    private PricesRepository netPriceRepositoryImpl;

    @Override
    public void getResultCheck() {
        showView.show("You got free: " + actionPizzaRepository.getAll());
        showView.show("Deliver: " + deliverPriceRepository.get());
        showView.show("Discount: " + discountPrice.get());
        showView.show("Total: " + totalPriceRepository.get());
    }


}
