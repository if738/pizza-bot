package com.student.Pizza.bot.handler.impl;

import com.student.Pizza.bot.entity.FreePizza;
import com.student.Pizza.bot.entity.Menu;
import com.student.Pizza.bot.entity.Type;
import com.student.Pizza.bot.handler.interfaces.FreePizzaHandler;
import com.student.Pizza.bot.repository.db.MenuEditRepository;
import com.student.Pizza.bot.repository.list_realisation.Impl.OrderedItemsRepositoryImpl;
import com.student.Pizza.bot.repository.list_realisation.interfaces.ItemsRepository;
import com.student.Pizza.bot.repository.list_realisation.interfaces.PricesRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.student.Pizza.bot.entity.Type.PIZZA;

@Component
@RequiredArgsConstructor
public class FreePizzaHandlerImpl implements FreePizzaHandler {

    private final MenuEditRepository menuEditRepository;
    private final OrderedItemsRepositoryImpl orderedItemsRepository;
    @Autowired
    @Qualifier("freePizzaRepositoryImpl")
    private final ItemsRepository<FreePizza> freePizzaRepository;
    @Autowired
    @Qualifier("totalPriceRepositoryImpl")
    private PricesRepository totalPriceRepository;

    @Override
    public void thirdPizzaIsFree() {
        if (compare()) {
            List<Menu> pizzas;
            pizzas = getCurrentOrder().stream()
                    .map(menuEditRepository::findByName)
                    .filter(menu -> menu.getType() == PIZZA).collect(Collectors.toList());
            getSortedPizzas(pizzas);
        }
    }

    private boolean compare() {
        long ordered = orderedItemsRepository.getAll().stream().filter(m -> m.getType() == Type.PIZZA).count();
        long freeCurrent = freePizzaRepository.getAll().size();
        return ordered / 3 > freeCurrent;
    }

    private void getSortedPizzas(List<Menu> pizzas) {
        if (pizzas.size() / 3 > 0) {
            List<Menu> sortedPizzas = pizzas.stream()
                    .sorted(Comparator.comparing(Menu::getPrice))
                    .collect(Collectors.toList());
            while (compare()) {
                int i = 0;
                i++;
                freePizzaRepository.put(new FreePizza(sortedPizzas
                        .get(0).getPrice(), sortedPizzas.get(0).getName()));
                sortedPizzas.remove(i);
                totalPriceRepository.delete(sortedPizzas.get(0).getPrice());
            }
//            discountSetter();
        }
    }

//    private void discountSetter() {
//        List<Float> discountPrice;
//        discountPrice = freePizzaRepository.getAll().stream().map(FreePizza::getPrice).collect(Collectors.toList());
//        float sum = (float) discountPrice.stream().mapToDouble(Float::longValue).sum();
//        ;
//    }


    private List<String> getCurrentOrder() {
        return orderedItemsRepository.getAll().stream().map(Menu::getName).collect(Collectors.toList());
    }


}
