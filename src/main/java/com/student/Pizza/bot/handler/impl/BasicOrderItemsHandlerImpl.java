package com.student.Pizza.bot.handler.impl;

import com.student.Pizza.bot.entity.Menu;
import com.student.Pizza.bot.entity.Type;
import com.student.Pizza.bot.handler.interfaces.*;
import com.student.Pizza.bot.repository.db.MenuEditRepository;
import com.student.Pizza.bot.repository.list_realisation.interfaces.ItemsRepository;
import com.student.Pizza.bot.repository.list_realisation.interfaces.PricesRepository;
import com.student.Pizza.bot.view.InputView;
import com.student.Pizza.bot.view.ShowView;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class BasicOrderItemsHandlerImpl implements BasicOrderItemsHandler {

    private final InputView inputView;
    private final ShowView showView;
    private final InputInfoHandler inputInfoHandler;
    private final AdditionaOrderlItemsHandler additionaOrderlItemsHandler;
    private final MenuEditRepository menuEditRepository;
    private final FreePizzaHandler freePizzaHandler;
    @Autowired
    @Qualifier("orderedItemsRepositoryImpl")
    private ItemsRepository<Menu> orderedItemsRepository;
    @Autowired
    @Qualifier("totalPriceRepositoryImpl")
    private PricesRepository totalPriceRepository;
    @Autowired
    @Qualifier("netPriceRepositoryImpl")
    private PricesRepository netPriceRepositoryImpl;

    @Override
    public void basicItemsHandler() {
        showView.show("Please input your order: ");
        String order = inputView.input().nextLine();
        List<Menu> basicOrder;
        basicOrder = inputInfoHandler.getValidItems(order).stream()
                .map(menuEditRepository::findByName).collect(Collectors.toList());
        orderedItemsRepository.putAll(basicOrder);
        isValidInput(order);
        setAdditionalPrices((float) basicOrder.stream().mapToDouble(Menu::getPrice).sum());
        freePizzaHandler.thirdPizzaIsFree();
        check();
    }

    private void check(){
        freePizzaHandler.thirdPizzaIsFree();
        if (((orderedItemsRepository.getAll().stream().filter(m -> m.getType() == Type.PIZZA).count() + 1) % 3) == 0){
            showView.show("-------------------------------------------------------------------------------" +
                    "\nWe have special offer \"Each third pizza is free\". " +
                    "\nYou may order one more pizza free, input pizzas or input more items");
            additionaOrderlItemsHandler.setMoreItems();
        }
    }

    private void setAdditionalPrices(float additionalPrice){
        netPriceRepositoryImpl.put(additionalPrice);
        totalPriceRepository.put(additionalPrice);
    }

    private void isValidInput(String order) {
        if (inputInfoHandler.getOrderInfo(order, false).size() > 0) {
            showView.show("-------------------------------------------------------------------------------" +
                    "\ncannot identify next inputs: "
                    + inputInfoHandler.getOrderInfo(order, false));
            showView.show("where valid items are: "
                    + inputInfoHandler.getOrderInfo(order, true));
            additionaOrderlItemsHandler.setMoreItems();
        }else showView.show("-------------------------------------------------------------------------------" +
                "\nyour current order are: " + orderedItemsRepository.getAll().stream().map(Menu::getName).collect(Collectors.joining(" ")));
    }

}
