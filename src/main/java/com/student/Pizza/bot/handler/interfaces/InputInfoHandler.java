package com.student.Pizza.bot.handler.interfaces;

import java.util.List;

public interface InputInfoHandler {

    List<String> getOrderInfo(String getStringFromConsole, boolean validity);

    List<String> getOrderInfo(String getStringFromConsole);

    List<String> getOrderInfo(List<String> order);

    List<String> getValidItems(String order);

}