package com.student.Pizza.bot.repository.db;

import com.student.Pizza.bot.entity.Menu;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MenuEditRepository extends JpaRepository<Menu,Long> {

   Menu findByName(String name);

}
