package com.student.Pizza.bot.repository.list_realisation.interfaces;

import org.springframework.stereotype.Repository;

@Repository
public interface PricesRepository {

        void put(Float putPrice);
        Float get();
        void delete(Float deletePrice);

}
