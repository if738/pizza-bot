package com.student.Pizza.bot.repository.db;

import com.student.Pizza.bot.entity.DiscountPercent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PercentEditRepository extends JpaRepository<DiscountPercent,Long> {

}
