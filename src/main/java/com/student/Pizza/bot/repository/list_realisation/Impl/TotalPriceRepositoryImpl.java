package com.student.Pizza.bot.repository.list_realisation.Impl;

import com.student.Pizza.bot.repository.list_realisation.interfaces.PricesRepository;
import org.springframework.stereotype.Repository;

@Repository
public class TotalPriceRepositoryImpl implements PricesRepository {

    private float memory;

    @Override
    public void put(Float putPrice) {
        memory = memory + putPrice;
    }

    @Override
    public Float get() {
        return memory;
    }

    @Override
    public void delete(Float deletePrice) {
        memory = memory - deletePrice;
    }

}
