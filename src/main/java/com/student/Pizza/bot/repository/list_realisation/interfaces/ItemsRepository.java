package com.student.Pizza.bot.repository.list_realisation.interfaces;

import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemsRepository<T> {

    void put(T entity);

    void putAll(List<T> entities);

    T getOne(long index);

    List<T> getAll();

}
