package com.student.Pizza.bot.repository.list_realisation.Impl;

import com.student.Pizza.bot.repository.list_realisation.interfaces.PricesRepository;
import org.springframework.stereotype.Repository;

@Repository
public class NetPriceRepositoryImpl implements PricesRepository {

    private float memory;

    @Override
    public void put(Float putPrice) {
        this.memory = this.memory + putPrice;
    }

    @Override
    public Float get() {
        return this.memory;
    }

    @Override
    public void delete(Float deletePrice) {

    }

}
