package com.student.Pizza.bot.repository.list_realisation.Impl;

import com.student.Pizza.bot.entity.Menu;
import com.student.Pizza.bot.repository.list_realisation.interfaces.ItemsRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class OrderedItemsRepositoryImpl implements ItemsRepository<Menu> {

    private List<Menu> memory = new ArrayList<>();

    @Override
    public void put(Menu entity) {
        memory.add(entity);
    }

    @Override
    public void putAll(List<Menu> entities) {
        memory.addAll(entities);
    }

    @Override
    public Menu getOne(long index) {
        return memory.get((int) index);
    }

    @Override
    public List<Menu> getAll() {
        return memory;
    }

}
