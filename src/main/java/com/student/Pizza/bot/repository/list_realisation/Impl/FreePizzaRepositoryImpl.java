package com.student.Pizza.bot.repository.list_realisation.Impl;

import com.student.Pizza.bot.entity.FreePizza;
import com.student.Pizza.bot.repository.list_realisation.interfaces.ItemsRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class FreePizzaRepositoryImpl implements ItemsRepository<FreePizza> {

    private List<FreePizza> db = new ArrayList<>();

    @Override
    public void put(FreePizza entity) {
        db.add(entity);
    }

    @Override
    public void putAll(List<FreePizza> entities) {
db.addAll(entities);
    }

    @Override
    public FreePizza getOne(long index) {
        return db.get((int) index);
    }

    @Override
    public List<FreePizza> getAll() {
        return db;
    }

}