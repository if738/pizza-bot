package com.student.Pizza.bot.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@Data
@AllArgsConstructor
public class FreePizza {

    private float price;
    private String name;

}
