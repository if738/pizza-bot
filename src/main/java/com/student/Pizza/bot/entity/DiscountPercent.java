package com.student.Pizza.bot.entity;

import lombok.Data;
import lombok.Getter;

import javax.persistence.*;

@Entity
@Data
@Table(name ="discount_percent")
public class DiscountPercent {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private int percent;

}
