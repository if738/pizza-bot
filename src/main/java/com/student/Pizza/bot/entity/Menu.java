package com.student.Pizza.bot.entity;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

import static javax.persistence.EnumType.STRING;

@Data
@Entity
@Table(name = "menu")
@Slf4j
public class Menu {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
//    private long created;
//    private long updated;
    @NotNull
    private String name;
    @NotNull
    private Float price;
    @NotNull
    @Enumerated(STRING)
    private Type type;

    @PrePersist
    private void create() {
//        setCreated(System.currentTimeMillis());
//        setUpdated(System.currentTimeMillis());
    }

    @PreUpdate
    private void update() {
//        setUpdated(System.currentTimeMillis());
    }

}
