package com.student.Pizza.bot.second_point;

import com.student.Pizza.bot.controller.pseudo.ControllerPseudo;
import com.student.Pizza.bot.handler.interfaces.FreePizzaHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class MyRunner implements CommandLineRunner {

    @Autowired
    @Qualifier("freePizzaHandlerImpl")
    FreePizzaHandler freePizzaHandler;
    @Autowired
    private ControllerPseudo def;

    @Override
    public void run(String... args) throws Exception {

        def.defaultChat();
        def.sentence();
        def.deliver();
        def.discount();
        def.check();

    }

}