package com.student.Pizza.bot.view.impl;

import com.student.Pizza.bot.view.InputView;
import org.springframework.stereotype.Component;

import java.util.Scanner;
@Component
public class InputViewImpl implements InputView {

    @Override
    public Scanner input() {
        Scanner scan = new Scanner(System.in);
        return scan;
    }

}
