package com.student.Pizza.bot.view.impl;

import com.student.Pizza.bot.view.ShowView;
import org.springframework.stereotype.Component;

@Component
public class ShowViewImpl implements ShowView {

    @Override
    public <T> void show(T t) {

        System.out.println(t);

    }

}
