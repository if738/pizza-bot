package com.student.Pizza.bot.view;

import java.util.List;

public interface  ShowView {

    <T> void show(T t);

}
