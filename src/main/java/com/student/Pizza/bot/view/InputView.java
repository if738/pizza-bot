package com.student.Pizza.bot.view;

import java.util.Scanner;

public interface InputView {

    Scanner input();

}
