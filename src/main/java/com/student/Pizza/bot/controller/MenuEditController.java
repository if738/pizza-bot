package com.student.Pizza.bot.controller;

import com.student.Pizza.bot.repository.db.MenuEditRepository;
import com.student.Pizza.bot.entity.Menu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/menu")
public class MenuEditController {

    @Autowired
    private MenuEditRepository repository;

    @GetMapping
    public List<Menu> getAll() {
        return repository.findAll();
    }

    @PostMapping
    public Menu create(@RequestBody Menu menu) {
        return repository.save(menu);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Menu> getById(@PathVariable("id") long id) {
        Optional<Menu> optionalMenu = repository.findById(id);
        if (optionalMenu.isPresent()) {
            return ResponseEntity.ok().body(optionalMenu.get());
        }
        return ResponseEntity.notFound().build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<Menu> updateMenu(@PathVariable Long id, @Valid @RequestBody Menu menuDetails) {
        Optional<Menu> optionalMenu = repository.findById(id);
        if (!optionalMenu.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        Menu menu = optionalMenu.get();
        menu.setName(menuDetails.getName());
        menu.setPrice(menuDetails.getPrice());
        menu.setType(menuDetails.getType());

        Menu updateMenu = repository.save(menu);
        return ResponseEntity.ok().body(updateMenu);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Menu> deleteMenu(@PathVariable long id) {
        Menu menu = repository.findById(id).orElse(null);
        if (menu == null) {
            ResponseEntity.notFound().build();
        }
        repository.deleteById(id);
        return ResponseEntity.ok().body(menu);
    }

}