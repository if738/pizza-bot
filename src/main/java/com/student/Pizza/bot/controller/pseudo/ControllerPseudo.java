package com.student.Pizza.bot.controller.pseudo;

import com.student.Pizza.bot.handler.impl.BasicOrderItemsHandlerImpl;
import com.student.Pizza.bot.handler.interfaces.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;

@Controller
@RequiredArgsConstructor
public class ControllerPseudo {

    private final BasicOrderItemsHandlerImpl requestHandler;
    private final FreePizzaHandler freePizzaHandler;
    private final DiscountHandler discountHandler;
    private final SentenceHandler sentenceHandler;
    private final DeliverHandler deliverHandler;
    private final CheckHandler checkHandler;

    public void defaultChat() {
        requestHandler.basicItemsHandler();
    }

    public void freePizza() {
        freePizzaHandler.thirdPizzaIsFree();
    }
    
    public void freePizzaCheck() {
        freePizzaHandler.thirdPizzaIsFree();
    }

    public void discount(){
        discountHandler.discount();
    }

    public void sentence(){
        sentenceHandler.sentence();
    }

    public void deliver(){
        deliverHandler.deliver();
    }

    public void check(){
        checkHandler.getResultCheck();
    }

}