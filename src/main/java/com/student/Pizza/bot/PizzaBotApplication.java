package com.student.Pizza.bot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PizzaBotApplication {

    public static void main(String[] args) {

        SpringApplication.run(PizzaBotApplication.class, args);

    }


}
