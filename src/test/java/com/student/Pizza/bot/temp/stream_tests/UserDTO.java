package com.student.Pizza.bot.temp.stream_tests;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserDTO {

    private Integer age;
    private String name;

}
