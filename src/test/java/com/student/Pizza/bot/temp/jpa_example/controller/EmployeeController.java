package com.student.Pizza.bot.temp.jpa_example.controller;

import com.student.Pizza.bot.temp.jpa_example.repository.EmployeeRepository;
import com.student.Pizza.bot.temp.jpa_example.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/company")
public class EmployeeController {

    //    @Autowired
//    private EmployeeDAO employeeDAO;
//
    @Autowired
    private EmployeeRepository repository;

    @PostMapping("/employees")
    public Employee createEmployee(@Valid @RequestBody Employee emp) {
        return repository.save(emp);
    }

    @GetMapping("/employees")
    public List<Employee> getAllEmployees() {
        return repository.findAll();
    }

    @GetMapping("/employees/{id}")
    public ResponseEntity<Employee> getEmployeeById(@PathVariable Long id) {
        Employee emp = repository.findById(id).orElse(null);
        if (emp == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(emp);
    }

    @PutMapping("/employees/{id}")
    public ResponseEntity<Employee> updateEmployee(@PathVariable("id") Long empId, @Valid @RequestBody Employee empDetails) {
        Employee emp = repository.findById(empId).orElse(null);
        if (emp == null) {
            return ResponseEntity.notFound().build();
        }
        emp.setName(empDetails.getName());
        emp.setDesignation(empDetails.getDesignation());
        emp.setExpertise(empDetails.getExpertise());

        Employee updateEmployee = repository.save(emp);

        return ResponseEntity.ok().body(updateEmployee);
    }

    @DeleteMapping("/notes/{id}")
    public ResponseEntity<Employee> deleteEmployee(@PathVariable("id") Long empId) {
        Employee emp = repository.findById(empId).orElse(null);

        if (emp == null) {
            return ResponseEntity.notFound().build();
        }

        repository.delete(emp);

        return ResponseEntity.ok().build();
    }

}
