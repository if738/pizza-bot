package com.student.Pizza.bot.temp.jpa_example.dao_useless_class;

import com.student.Pizza.bot.temp.jpa_example.repository.EmployeeRepository;
import com.student.Pizza.bot.temp.jpa_example.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeDAO {

    @Autowired
    private EmployeeRepository employeeRepository;

    //to save an employee
    public Employee save(Employee emp) {
        return employeeRepository.save(emp);
    }

    //to search an employee
    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }

    //get an employee by id
    public Employee findOne(Long empId) {
        return employeeRepository.findById(empId).orElse(null);
    }

    //delete an employee by id
    public void delete(Employee emp) {
        employeeRepository.delete(emp);
    }
}
