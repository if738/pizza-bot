package com.student.Pizza.bot.temp.string_value_of;

import java.util.Scanner;

public class StringValueOfRegex {

    public static void main(String[] args) {
//        Scanner scan = new Scanner(System.in);
//        String text = scan.nextLine();
//        Integer cardNumber = Integer.valueOf(text.replaceAll("\\d",""));
//        System.out.println(cardNumber);

        String s2 = "161651615";
        Integer s3 = Integer.valueOf(s2.replaceAll("\\D|\\s","9"));
        System.out.println(s3);

        String s4 = "       ";
        Integer s5 = Integer.valueOf(s4.replaceAll("\\D","9"));
        System.out.println(s5);

        String s6 = "dg 45 d 4";
        Long s7 = Long.valueOf(s6.replaceAll("\\D","9"));
        System.out.println(s7);

        String s8 = "fasdg";
        Integer s9 = Integer.valueOf(s8.replaceAll("\\D","9"));
        System.out.println(s9);

        String s10 = " asdf ";
        Integer s11 = Integer.valueOf(s10.replaceAll("\\D","9"));
        System.out.println(s11);

        String s = "";
        String s1 = s.replaceAll("\\D","9");
        System.out.println(s1);

    }

}
