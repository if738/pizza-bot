package com.student.Pizza.bot.temp.service_testing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AllUsersList {

    @Autowired
    private User user;

    @GetMapping("/take")
    public String getUser(){
        return user.email + " " + user.name;
    }

}
