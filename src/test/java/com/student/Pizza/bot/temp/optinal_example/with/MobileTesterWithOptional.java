package com.student.Pizza.bot.temp.optinal_example.with;


import com.student.Pizza.bot.temp.optinal_example.ScreenResolution;

import static java.util.Optional.*;

public class MobileTesterWithOptional {

    public static void main(String[] args) {
        ScreenResolution resolution = new ScreenResolution(750,1334);
        DisplayFeatures dfeatures = new DisplayFeatures("4.7", of(resolution));
        Mobile mobile = new Mobile(2015001, "Apple", "iPhone 6s", of(dfeatures));

        MobileService mService = new MobileService();

        int width = mService.getMobileScreenWidth(of(mobile));
        System.out.println("Apple iPhone 6s Screen Width = " + width);

        Mobile mobile2 = new Mobile(2015001, "Apple", "iPhone 6s", empty());
        int width2 = mService.getMobileScreenWidth(of(mobile2));
        System.out.println("Apple iPhone 16s Screen Width = " + width2);
    }

}
