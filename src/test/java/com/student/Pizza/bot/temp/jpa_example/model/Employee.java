package com.student.Pizza.bot.temp.jpa_example.model;

import lombok.Data;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Data
@Entity
@Table(name = "Employees")
@EntityListeners(AuditingEntityListener.class)
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotBlank
    @Column(name = "name")
    private String name;

    @NotBlank
    private String designation;

    @NotBlank
    private String expertise;

    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date createdAt;

}
