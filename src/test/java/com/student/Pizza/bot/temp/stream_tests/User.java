package com.student.Pizza.bot.temp.stream_tests;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@AllArgsConstructor
public class User {

    private Integer age;
    private String name;

}
