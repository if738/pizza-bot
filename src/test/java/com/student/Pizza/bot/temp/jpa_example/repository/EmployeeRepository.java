package com.student.Pizza.bot.temp.jpa_example.repository;

import com.student.Pizza.bot.temp.jpa_example.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee,Long> {
}
