package com.student.Pizza.bot.temp.optinal_example.with;

import com.student.Pizza.bot.temp.optinal_example.ScreenResolution;

import java.util.Optional;

public class MobileService {

    public Integer getMobileScreenWidth(Optional<Mobile> mobile){

        return mobile.flatMap(Mobile::getDisplayFeatures)
                .flatMap(DisplayFeatures::getResolution)
                .map(ScreenResolution::getWidth).orElse(255);

    }

}
